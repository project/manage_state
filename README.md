## Manage State

Module allows you to manage everything that is currently stored in the
State API system. Simply provides an interface/overview to list out all
available State API variables currently configured on the site. From
here, one can simply view the data stored inside the variable or can
remove the state variable & it's information all together.


### Features

* State variables overview page.
* Search and filter state variables on overview page.
* Edit state variables and their values
  (only simple and less complex variables).
* Delete state variables individually.
* Delete selected state variables.
* Delete all state variables in the system (be careful using this feature).

**Not recommended for production sites, but use at your own discretion.**


### Requirements

* Requires symfony/var-dumper library.
* No dependencies on devel module.


### Install/Usage

* Install module like any other contributed module.
* No configuration is necessary.
* Once installed, you can visit the state variables overview page:
  **/admin/config/development/state**
* Manage state variables how you see fit.


### Similar Modules

Devel module provides an interface to list out State API variables, but you
cannot delete state variables. This module provides a bit more functionality
around managing stored state information.


### Roadmap

* Add in pretty var dumper similar to devel.
* Add in drush commands to manage state variables.


### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
