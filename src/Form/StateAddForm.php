<?php

namespace Drupal\manage_state\Form;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form API form to add a state.
 */
class StateAddForm extends FormBase {

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a new StateEditForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'manage_state_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Store in the form the name of the state variable.
    $form['state_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('State machine name'),
      '#required' => TRUE,
    ];

    // Set the transport format for the new value.
    $form['transport'] = [
      '#type' => 'select',
      '#title' => $this->t('Transport type'),
      '#options' => [
        '' => $this->t('- Select Type -'),
        'plain' => $this->t('Plain'),
        'yaml' => $this->t('YAML'),
      ],
      '#description' => $this->t('Simple data types, use plain. For more complex data types like arrays, use YAML.'),
      '#default_value' => 'plain',
      '#required' => TRUE,
    ];

    $form['state_value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('State Value'),
      '#rows' => 15,
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => Url::fromRoute('manage_state.state_overview'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if ($values['transport'] == 'yaml') {
      // Try to parse the new provided value.
      try {
        $parsed_value = Yaml::decode($values['state_value']);
        $form_state->setValue('parsed_value', $parsed_value);
      }
      catch (InvalidDataTypeException $e) {
        $form_state->setErrorByName('state_value', $this->t('Invalid input: %error', ['%error' => $e->getMessage()]));
      }
    }
    else {
      $form_state->setValue('parsed_value', $values['state_value']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save the state.
    $values = $form_state->getValues();
    $this->state->set($values['state_name'], $values['parsed_value']);

    // Redirect back to state overview page.
    $form_state->setRedirectUrl(Url::fromRoute('manage_state.state_overview'));

    // Add message and log.
    $this->messenger()->addMessage($this->t('Variable %variable was successfully created.', ['%variable' => $values['state_name']]));
    $this->logger('manage_state')->info('Variable %variable was successfully created.', ['%variable' => $values['state_name']]);
  }

  /**
   * Helper function to determine if a variable is or contains an object.
   *
   * @param mixed $data
   *   Input data to check.
   *
   * @return bool
   *   TRUE if the variable is not an object and does not contain one.
   */
  protected function checkObject($data) {
    if (is_object($data)) {
      return FALSE;
    }
    if (is_array($data)) {
      // If the current object is an array, then check recursively.
      foreach ($data as $value) {
        // If there is an object the whole container is "contaminated".
        if (!$this->checkObject($value)) {
          return FALSE;
        }
      }
    }

    // All checks pass.
    return TRUE;
  }

}
