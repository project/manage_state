<?php

namespace Drupal\manage_state\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactory;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to delete all state variables.
 */
class StateDeleteAllForm extends ConfirmFormBase {

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Key value store factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactory
   */
  protected $keyValue;

  /**
   * Constructs a new StateDeleteAllForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactory $keyValue
   *   The key value store factory service.
   */
  public function __construct(StateInterface $state, KeyValueFactory $keyValue) {
    $this->state = $state;
    $this->keyValue = $keyValue;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('keyvalue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "manage_state_delete_all_form";
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Loop over all state variables and delete one by one.
    $deleted = 0;
    foreach ($this->keyValue->get('state')->getAll() as $state_name => $state) {
      $this->state->delete($state_name);
      $deleted++;
    }

    // Success message.
    $this->messenger()->addMessage($this->t('%deleted State variables were deleted successfully.', ['%deleted' => $deleted]));

    // Back to state overview page.
    $form_state->setRedirect('manage_state.state_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('manage_state.state_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to delete all State variables?');
  }

}
