<?php

namespace Drupal\manage_state\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to delete state variable.
 */
class StateDeleteForm extends ConfirmFormBase {

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Holds name of state variable.
   *
   * @var string
   */
  protected $stateName;

  /**
   * Constructs a new StateDeleteForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "manage_state_delete_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $state_name = '') {
    $this->stateName = $state_name;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Delete state variable.
    $this->state->delete($this->stateName);

    // Success message.
    $this->messenger()->addMessage($this->t('State variable %state_name was deleted successfully.', ['%state_name' => $this->stateName]));

    // Back to state overview page.
    $form_state->setRedirect('manage_state.state_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('manage_state.state_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to delete State variable %state_name?', ['%state_name' => $this->stateName]);
  }

}
