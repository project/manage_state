<?php

namespace Drupal\manage_state\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\KeyValueStore\KeyValueFactory;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a confirmation form to delete multiple state variables.
 */
class StateDeleteMultipleForm extends FormBase {

  /**
   * The state store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Key value store factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactory
   */
  protected $keyValue;

  /**
   * Constructs a new StateDeleteMultipleForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactory $keyValue
   *   The key value store factory service.
   */
  public function __construct(StateInterface $state, KeyValueFactory $keyValue) {
    $this->state = $state;
    $this->keyValue = $keyValue;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('keyvalue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "manage_state_delete_multiple_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $header = [
      'name' => $this->t('Name'),
      'size' => $this->t('Estimated Size'),
    ];

    $rows = [];
    foreach ($this->keyValue->get('state')->getAll() as $state_name => $state) {
      $rows[$state_name] = [
        'name' => $state_name,
        'size' => $this->t('%state_size KB', ['%state_size' => $this->getStateSize($state)]),
      ];
    }

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $rows,
      '#empty' => $this->t('No state variables were found.'),
    ];

    if ($rows) {
      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete state variables'),
      ];
    }

    return $form;
  }

  /**
   * Get size of state variable value.
   *
   * @param mixed $state
   *   The state variable.
   *
   * @return float|int
   *   Returns the size.
   */
  private function getStateSize($state) {
    $size = strlen(var_export($state, TRUE));
    return ($size * 8 / 1000);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $states = array_filter($values['table']);

    $deleted = 0;
    foreach ($states as $state_name) {
      $this->state->delete($state_name);
      $deleted++;
    }

    // Success message.
    $this->messenger()->addMessage($this->t('%deleted State variables were deleted successfully.', ['%deleted' => $deleted]));

    // Back to state overview page.
    $form_state->setRedirect('manage_state.state_overview');
  }

}
