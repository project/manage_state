<?php

namespace Drupal\manage_state\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Field\FormatterPluginManager;
use Drupal\Core\Field\WidgetPluginManager;
use Drupal\Core\Url;
use Drupal\Core\Utility\TableSort;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns response for managing and listing state data.
 */
class StateController extends ControllerBase {

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The field type plugin manager service.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManagerInterface
   */
  protected $fieldTypeManager;

  /**
   * The field formatter plugin manager.
   *
   * @var \Drupal\Core\Field\FormatterPluginManager
   */
  protected $formatterPluginManager;

  /**
   * The field widget plugin manager.
   *
   * @var \Drupal\Core\Field\WidgetPluginManager
   */
  protected $widgetPluginManager;

  /**
   * StateController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type manager service.
   * @param \Drupal\Core\Field\FormatterPluginManager $formatter_plugin_manager
   *   The field formatter plugin manager.
   * @param \Drupal\Core\Field\WidgetPluginManager $widget_plugin_manager
   *   The field widget plugin manager.
   */
  public function __construct(EntityTypeBundleInfoInterface $entity_type_bundle_info, FieldTypePluginManagerInterface $field_type_manager, FormatterPluginManager $formatter_plugin_manager, WidgetPluginManager $widget_plugin_manager) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->fieldTypeManager = $field_type_manager;
    $this->formatterPluginManager = $formatter_plugin_manager;
    $this->widgetPluginManager = $widget_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.bundle.info'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('plugin.manager.field.formatter'),
      $container->get('plugin.manager.field.widget')
    );
  }

  /**
   * Builds the state variable overview page.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return array
   *   Array of page elements to render.
   */
  public function stateOverview(Request $request) {
    $can_edit = $this->currentUser()->hasPermission('administer site configuration');

    $header = [
      [
        'data' => $this->t('Name'),
        'field' => 'name',
        'sort' => 'asc',
      ],
      [
        'data' => $this->t('Estimated Size'),
        'field' => 'size',
      ],
      [
        'data' => $this->t('Value'),
        'field' => 'value',
      ],
    ];

    if ($can_edit) {
      $header['edit'] = $this->t('Operations');
    }

    $rows = [];
    // State class doesn't have getAll method so we get all states from the
    // KeyValueStorage.
    foreach ($this->keyValue('state')->getAll() as $state_name => $state) {
      $rows[$state_name] = [
        'name' => [
          'data' => $state_name,
          'class' => 'table-filter-text-source',
        ],
        'size' => [
          'data' => $this->t('%state_size KB', ['%state_size' => $this->getStateSize($state)]),
          'class' => 'state-size-column',
        ],
        'value' => [
          'data' => var_export($state, TRUE),
          'class' => 'state-value-column',
        ],
      ];

      if ($can_edit) {
        $operations['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute('manage_state.state_edit', ['state_name' => $state_name]),
        ];
        $operations['delete'] = [
          'title' => $this->t('Delete'),
          'url' => Url::fromRoute('manage_state.state_delete', ['state_name' => $state_name]),
        ];
        $rows[$state_name]['edit'] = [
          'data' => ['#type' => 'operations', '#links' => $operations],
        ];
      }
    }

    // Sort the table according to the options passed in the query arguments.
    $sort = TableSort::getSort($header, $request);
    $order_by = TableSort::getOrder($header, $request)['sql'];

    // Sort rows.
    usort($rows, function (array $a, array $b) use ($sort, $order_by): int {
      $result = $a[$order_by] <=> $b[$order_by];
      return $sort === 'asc' ? $result : -$result;
    });

    $output['total_states'] = [
      '#markup' => $this->t('%total_states state variable(s) found.', ['%total_states' => count($rows)]),
    ];

    $output['states'] = [
      '#type' => 'state_table_filter',
      '#filter_label' => $this->t('Search'),
      '#filter_placeholder' => $this->t('Enter state name'),
      '#filter_title' => $this->t('Enter a part of the state name to filter by.'),
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No state variables found.'),
      '#attributes' => [
        'class' => ['manage-state-list'],
      ],
    ];

    return $output;
  }

  /**
   * Get size of state variable value.
   *
   * @param mixed $state
   *   The state variable.
   *
   * @return float|int
   *   Returns size.
   */
  private function getStateSize($state) {
    $size = strlen(var_export($state, TRUE));
    return ($size * 8 / 1000);
  }

}
